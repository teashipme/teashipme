let generator = document.getElementById('generator');

let generate = document.getElementById('generate');
let inputColor = document.getElementById('inputColor');
let inputOpacity = document.getElementById('inputOpacity');
let inputBackground = document.getElementById('inputBackground');
let inputBorder = document.getElementById('inputBorder');
let inputSize = document.getElementById('inputSize');
let inputQuantity = document.getElementById('inputQuantity');
let inputRotate = document.getElementById('inputRotate');

generate.addEventListener('click', function(){

    generator.innerHTML = '';
    generator.style.backgroundColor = inputBackground.value;

    let random = Math.floor(Math.random() * inputQuantity.value);

    for(let i=0;i<random;i++){

        let el = document.createElement('div');
        el.classList.add('point');
        let randomEl = Math.floor(Math.random() * inputSize.value);
        let randomPositionTop = Math.floor(Math.random() * 500);
        let randomPositionLeft = Math.floor(Math.random() * 500);

        console.log(randomEl, randomPositionTop, randomPositionLeft)
            
            el.style.cssText = `
                background: ${inputColor.value};
                opacity: ${inputOpacity.value * 0.1};
                top: ${randomPositionTop}px;
                left: ${randomPositionLeft}px;
                width: ${randomEl}px;
                height: ${randomEl}px;
                border-radius: ${inputBorder.value}%;
                transform: rotate(${inputRotate.value}deg);
            `;

            generator.appendChild(el);     
    }
})